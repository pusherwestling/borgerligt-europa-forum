<li>
	<!-- ARTICLE ITEM => PAGE -->
	<article <?php post_class("article--page"); ?> id="article-<?php the_ID(); ?>">
		<!-- HEADER -->
		<header class="article__header">
			<h1 class="article__headline"><?php the_title(); ?></h1>
		</header>

		<!-- CONTENT -->
		<section class="article__content">
			<?php the_content(); ?>
		</section>
	</article>
</li>
