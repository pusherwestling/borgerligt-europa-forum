<nav class="article__options">
	<?php if(get_previous_post_link()): ?>
		<?php previous_post_link('%link'); ?>
	<?php endif; ?>

	<?php if(get_next_post_link()): ?>
		<?php next_post_link('%link'); ?>
	<?php endif; ?>
</nav>
