<li>
	<!-- ARTICLE ITEM => POST PREVIEW -->
	<article <?php post_class("article--post-preview"); ?> id="article-<?php the_ID(); ?>">
		<!-- HEADER -->
		<header class="article__header">
			<?php grund_article_date(); ?>
			<h2 class="article__headline">
				<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h2>
		</header>

		<!-- EXCERPT -->
		<section class="article__excerpt">
			<?php the_excerpt(); ?>
		</section>
	</article>
</li>
