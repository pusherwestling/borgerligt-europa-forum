<li>
	<!-- ARTICLE ITEM => POST -->
	<article <?php post_class("article--post"); ?> id="article-<?php the_ID(); ?>">
		<!-- HEADER -->
		<header class="article__header">
			<h1 class="article__headline"><?php the_title(); ?></h1>
			<?php grund_article_date(); ?>
		</header>

		<!-- CONTENT -->
		<section class="article__content">
			<?php the_content(); ?>
		</section>

		<?php if(is_single()): ?>
			<?php get_template_part("blocks/article/post", "options"); ?>
		<?php endif; ?>
	</article>
</li>
