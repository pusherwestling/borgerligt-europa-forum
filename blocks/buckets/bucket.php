<li>
	<!-- BUCKET -->
	<article class="bucket">
		<header class="bucket__header">
			<h1 class="bucket__headline"><?php the_title(); ?></h1>
		</header>
		<section class="bucket__content">
			<?php the_content(); ?>
		</section>
	</article>
</li>
