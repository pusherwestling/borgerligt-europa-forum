<script type="text/javascript">
var heroes = new Vue({
	el: '#heroes',
	data: {
		heroes: [],
		hero: 1
	},
	methods: {
		initHero: function(index){
			if(this.heroes.indexOf(index) < 0){
				console.log("Added "+index+" to hero stack");
				this.heroes.push(index);
			}
		},
		prevHero: function(){
			var current = this.hero;

			if(this.hero == 1){
				this.hero = this.heroes.length;
			} else {
				this.hero--;
			}
		},
		nextHero: function(){
			var current = this.hero;

			if(this.hero == this.heroes.length){
				this.hero = 1;
			} else {
				this.hero++;
			}
		},
		gotoHero: function(index){
			if(index >= 1 && index <= this.heroes.length){
				this.hero = index;
			}
		}
	}
});

Vue.directive('hero', function(index){
	console.log("Added "+index+" to hero stack", this);
	this.data.heroes += 1;
});
</script>
