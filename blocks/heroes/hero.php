<!-- HERO -->
<article class="hero{{ (hero == '<?php echo $index; ?>') ? '--show' : '' }}" style="background-image:url(<?php grund_article_image(); ?>)">
	{{ initHero(<?php echo $index; ?>) }}
	<div class="layout__container">
		<header class="hero__header">
			<h1 class="hero__headline"><?php the_title(); ?></h1>
		</header>
	</div>
</article>
