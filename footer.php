		<footer class="footer">
			<div class="layout__container">
				<section class="widgets">
					<ul class="widgets__list">
						<?php grund_widgets("footer"); ?>
					</ul>
				</section>
			</div>
		</footer>

		<?php wp_footer(); ?>
	</div>
</body>
</html>
