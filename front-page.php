<?php get_header(); ?>

	<!--
	/********************************/
	/*	front-page.php 				*/
	/********************************/
	-->

	<!-- HEROES -->
	<section class="heroes" id="heroes">
		<ul class="heroes__list">
			<?php grund_heroes(); ?>
		</ul>

		<div class="heroes__controls">
			<div class="layout__container">
				<?php get_template_part("blocks/heroes/prev"); ?>
				<ul class="heroes__slides">
					<?php grund_heroes_slides(); ?>
				</ul>
				<?php get_template_part("blocks/heroes/next"); ?>
			</div>
		</div>

		<?php get_template_part("blocks/heroes/functions"); ?>
	</section>

	<!-- CONTENT -->
	<section class="content">
		<div class="layout__container">
			<!-- BUCKETS -->
			<section class="buckets">
				<ul class="buckets__list">
					<?php grund_buckets(); ?>
				</ul>
			</section>
		</div>

		<div class="layout__container--flex">
			<!-- ARTICLES -->
			<section class="articles">
				<header class="articles__header">
					<h1 class="articles__headline"><?php _e("Latest news", "grund"); ?></h1>
				</header>
				<ul class="articles__list">
					<?php grund_articles_filters(3); ?>
					<?php grund_articles("preview"); ?>
				</ul>
			</section>

			<!-- WIDGETS -->
			<section class="widgets">
				<ul class="widgets__list">
					<?php grund_widgets(); ?>
				</ul>
			</section>
		</div>
	</section>

<?php get_footer(); ?>
