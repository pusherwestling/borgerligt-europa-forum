<?php

@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');

function grund_init(){
	include_once('functions/customizer.php');
	include_once('functions/navigations.php');
	include_once('functions/widgets.php');
	include_once('functions/excerpts.php');

	// Grund wordpress
	add_theme_support('automatic-feed-links');
	add_theme_support('post-thumbnails');
	add_custom_background();

	add_image_size("hero", 1920, 9999);

	// Grund languages
	load_theme_textdomain('grund', get_template_directory().'/languages');

	// Grund customizer
	grund_customizer_init();

	// Grund navigations
	grund_navigations_init();

	// Grund widgets
	grund_widgets_init();

	// Grund excerpts
	grund_excerpts_init();

	// Dependencies
	wp_enqueue_script("vue", get_template_directory_uri()."/assets/js/vue.js");
}
add_action('after_setup_theme', 'grund_init');

//
//	NAVIGATION
//

function grund_navigation($id){
	$config = array(
		'echo'				=> 0,
		'theme_location'	=> $id,
		'container'			=> false,
		'menu_class'		=> false,
		'items_wrap'		=> '%3$s',
		'depth'				=> 1
	);

	$markup = wp_nav_menu($config);
	$markup = str_replace("sub-menu", "sub-menu navigation__item__children", $markup);
	echo $markup;
}

//
//	ARTICLES
//

function grund_articles($type = false){
	if(have_posts()){
		while(have_posts()){
			the_post();

			if($type){
				get_template_part("blocks/articles/".get_post_type(), $type);
			} else {
				get_template_part("blocks/articles/".get_post_type());
			}
		}
	} else {
		get_template_part("blocks/system/no-content.php");
	}
}

function grund_articles_set_limit($limit){
	if($limit){
		query_posts(array(
			'posts_per_page' => $limit
		));
	}
}

function grund_articles_filters($limit = false, $hero = false){
	$arguments = array();

	if($limit){
		$arguments['posts_per_page'] = $limit;
	}

	if($hero){
		$tag = get_term_by('name', 'hero', 'post_tag');
		$tag_id = $tag->term_taxonomy_id;

		if($tag_id){
			$arguments['tag__not_in'] = array($tag_id);
		}
	}

	if(count($arguments) > 0){
		query_posts($arguments);
	}
}

function grund_article_date(){
	$markup = '<time class="article__date" datetime="%1$s">%2$s</time>';
	$markup = sprintf($markup,
		esc_attr(get_the_modified_date('c')),
		get_the_modified_date()
	);
	echo $markup;
}

function grund_article_image(){
	$image = get_template_directory_uri()."/assets/img/article-image-fallback.jpg";

	if(has_post_thumbnail(get_the_ID())){
		$image_data = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'hero');
		$image = $image_data[0];
	}

	echo $image;
}


//
//	HEROES
//

function grund_heroes($limit = 3, $template = "hero"){
	$arguments = array(
		'tag' => 'hero',
		'posts_per_page' => $limit
	);

	$heroes = new WP_Query($arguments);

	if($heroes->have_posts()){
		while($heroes->have_posts()){
			$heroes->the_post();
			$index = $heroes->current_post + 1;

			include(locate_template("blocks/heroes/".$template.".php"));
		}
	}

	wp_reset_postdata();
}

function grund_heroes_slides($limit = 3){
	grund_heroes($limit, "indicator");
}

//
//	BUCKETS
//

function grund_buckets($limit = 3){
	for($index = 1; $index <= $limit; $index++){
		$page_id = get_theme_mod("bucket_".$index);

		if($page_id){
			$arguments = array(
				'page_id' => $page_id
			);

			$bucket = new WP_Query($arguments);

			if($bucket->have_posts()){
				while($bucket->have_posts()){
					$bucket->the_post();
					get_template_part("blocks/buckets/bucket");
				}
			}

			wp_reset_postdata();
		}
	}
}

//
//	WIDGETS
//

function grund_widgets($type = false){
	if($type != false){
		dynamic_sidebar($type.'-widgets');
		return;
	}

	if(is_front_page()){
		// Display front-page widgets.
		$type = "home";
	} elseif(is_page()){
		// Display page widgets.
		$type = "page";
	} else {
		// Display category widgets.
		$type = "category";
	}

	dynamic_sidebar($type.'-widgets');
}




















/*


//
//	NAVIGATIONS
//

function grund_get_navigations($id){
	$config = array(
		'echo'				=> 0,
		'theme_location'	=> $id,
		'container'			=> false,
		'menu_class'		=> false,
		'items_wrap'		=> '%3$s',
		'depth'				=> 1
	);

	$markup = wp_nav_menu($config);

	$markup = str_replace("sub-menu", "sub-menu navigations__list__item__children", $markup);

	echo $markup;
}

//
//	HEROES
//

function grund_get_heroes($limit){
	$category_slug = "bildrotator";

	query_posts("category_name=".$category_slug."&showposts=".$limit);

	// Items
	if(have_posts()){
		while(have_posts()){
			the_post();
			get_template_part("blocks/heroes/item");
		}
	}

	// Controls
	get_template_part("blocks/heroes/control");

	wp_reset_postdata();
}

//
//	THREE LITTLE BUCKETS
//

function grund_get_bucket($index){
	$page_id = 	get_theme_mod("bucket_".$index);

	query_posts("page_id=".$page_id);

	if(have_posts()){
		while(have_posts()){
			the_post();
			get_template_part("blocks/buckets/item");
		}
	} else {
		echo "Page with id: ".$page_id." could not be found.";
	}

	wp_reset_postdata();
}

function grund_get_buckets($limit){
	for($i = 1; $i <= $limit; $i++){
		grund_get_bucket($i);
	}
}

//
//	ARTICLES
//

function grund_get_articles($limit = false){
	$arguments = array();

	if($limit) {
		$arguments['posts_per_page'] = $limit;
	}

	$grund_articles = new WP_Query($arguments);

	if($grund_articles->have_posts()){
		while($grund_articles->have_posts()){
			$grund_articles->the_post();
			get_template_part("blocks/articles/item", "excerpt");
		}
	} else {
		get_template_part("blocks/articles/none");
	}

	wp_reset_postdata();
}

function grund_get_article_date(){
	$markup = '<time class="articles__list__item__date" datetime="%1$s">%2$s</time>';
	$markup = sprintf($markup,
		esc_attr(get_the_modified_date('c')),
		get_the_modified_date()
	);

	echo $markup;
}


//
//	ARTICLES => FEATURED IMAGE
//

*/
