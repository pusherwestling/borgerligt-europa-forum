<?php

/*
*
*	THEME CUSTOMIZER
*	
*	Create page in wordpress admin to change common theme settings.
*
*/

function grund_customizer_init(){
	add_action("customize_register", "grund_customizer_registrator", 11);
}

function grund_customizer_registrator($wp_customize){
	$wp_customize->remove_section('colors');
	$wp_customize->remove_section('background_image');
	
	// Create panel
	$wp_customize->add_panel('grund', array(
		'title' 		=> __('Theme settings', 'grund')
	));
	
	// Add three little buckets section.
	$wp_customize->add_section('three_little_buckets', array(
		'title' 		=> __('Three little buckets', 'grund'),
		'capability' 	=> 'edit_theme_options'
	));
	
	// Add buckets.
	for($bucket = 1; $bucket <= 3; $bucket++){
		$wp_customize->add_setting('bucket_'.$bucket, array(
			'default'		=> '',
			'capability' 	=> 'edit_theme_options'
		));

		$wp_customize->add_control('bucket_'.$bucket, array(
			'section' 		=> 'three_little_buckets',
			'type' 			=> 'dropdown-pages',
			'label' 		=> __('Bucket '.$bucket, 'grund'),
			'input_attrs' => array(
				'placeholder' 		=> __('Enter a page id', 'grund'),
			)
		));
	}
}
