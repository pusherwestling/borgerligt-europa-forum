<?php

/*
*
*	THEME EXCERPTS
*
*	Various helper functions for excerpts in theme.
*
*/

function grund_excerpts_init(){
	add_filter('wp_trim_excerpt', 'grund_excerpts_trim', 10, 2);
}

function grund_excerpts_trim($text, $raw_excerpt){
	if(!$raw_excerpt){
		// Strip wordpress shortcodes.
		$content = apply_filters('the_content', strip_shortcodes(get_the_content()));

		// Use only the text from the first paragraph.
		$text = substr($content, 0, strpos($content, '</p>') + 4);

		// Remove all tags but some.
		$text = strip_tags($text, "<a><strong><em><b><i>");
	}

	return grund_excerpts_truncate($text);
}

function grund_excerpts_truncate($text, $limit = 170, $break = " ", $pad = "..."){
	if(strlen($text) <= $limit){
		return $text;
	}

	if(($breakpoint = strpos($text, $break, $limit)) !== false){
		if($breakpoint < (strlen($text) - 1)){
			$text = substr($text, 0, $breakpoint);

			// Prevent more than for dots as pad.
			if(substr($text, -1) == "." && $pad == "..."){
				$pad = "..";
			}

			$text = $text.$pad;

			// Add read more link.
			$text = $text.' <a class="article__read-more" href="'.get_the_permalink().'" title="'.__("Read more", "grund").': '.get_the_title().'">['.__("Read more", "grund").']</a>';
		}
	}

	return $text;
}
