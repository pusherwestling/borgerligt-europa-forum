<?php

/*
*
*	THEME NAVIGATIONS
*
*	Various helper functions for menus in theme.
*
*/

function grund_navigations_init(){
	// Registers all theme menus that can be utilized.
	register_nav_menus(array(
		'navigation' => __('Navigation', 'grund')
	));

	// Add custom theme classes for the menu items.
	add_filter('nav_menu_css_class', 'grund_navigations_item_class', 10, 2);
}

function grund_navigations_item_class($classes, $item){
	if(in_array('current-menu-item', $classes) || in_array('current-menu-parent', $classes) || in_array('current-menu-ancestor', $classes)){
		if(in_array('current-menu-item', $classes)){
			// Current menu item class.
			$classes[] = "navigation__item--current";
		}

		if(in_array('current-menu-parent', $classes)){
			// Parent menu item class.
			$classes[] = "navigation__item--parent";
		}

		if(in_array('current-menu-ancestor', $classes)){
			// Ancestor menu item class.
			$classes[] = "navigation__item--ancestor";
		}
	} else {
		// Default menu item class.
		$classes[] = "navigation__item";
	}

	return $classes;
}
