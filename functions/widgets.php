<?php

/*
*
*	THEME WIDGETS
*
*	Various helper functions for widgets in theme.
*
*/

function grund_widgets_init(){
	add_action('widgets_init', 'grund_widgets_registration');
}

function grund_widgets_registration(){
	// Home widgets
	register_sidebar(array(
		'name'          => __('Home widgets', 'grund'),
		'id'            => 'home-widgets',
		'class'         => 'widgets--home',
		'before_widget' => '<li><article class="widget %2$s" id="%1$s">',
		'after_widget'  => '</article></li>',
		'before_title'  => '<header><h3 class="widget__headline">',
		'after_title'   => '</h3></header>'
	));

	// Category widgets
	register_sidebar(array(
		'name'          => __('Category widgets', 'grund'),
		'id'            => 'category-widgets',
		'class'         => 'widgets--category',
		'before_widget' => '<li><article class="widget %2$s" id="%1$s">',
		'after_widget'  => '</article></li>',
		'before_title'  => '<header><h3 class="widget__headline">',
		'after_title'   => '</h3></header>'
	));

	// Page widgets
	register_sidebar(array(
		'name'          => __('Page widgets', 'grund'),
		'id'            => 'page-widgets',
		'class'         => 'widgets--page',
		'before_widget' => '<li><article class="widget %2$s" id="%1$s">',
		'after_widget'  => '</article></li>',
		'before_title'  => '<header><h3 class="widget__headline">',
		'after_title'   => '</h3></header>'
	));

	// Footer widgets
	register_sidebar(array(
		'name'          => __('Footer widgets', 'grund'),
		'id'            => 'footer-widgets',
		'class'         => 'widgets--footer',
		'before_widget' => '<li><article class="widget %2$s" id="%1$s">',
		'after_widget'  => '</article></li>',
		'before_title'  => '<header><h3 class="widget__headline">',
		'after_title'   => '</h3></header>'
	));
}
