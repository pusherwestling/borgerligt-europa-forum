<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Description goes here">
    <meta name="keywords" content="Keywords goes here">

	<link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">

    <?php wp_head(); ?>
</head>
<body>
	<div class="layout">
		<div id="navigation">
			<!-- HEADER -->
			<header class="header">
				<div>
					<a class="logo--small" href="<?php echo esc_url(home_url("/")); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri()."/assets/img/SmallLogoNormal.svg"; ?>" alt="<?php bloginfo("name"); ?>">
					</a>

					<a class="logo--large" href="<?php echo esc_url(home_url("/")); ?>" rel="home">
						<img src="<?php echo get_template_directory_uri()."/assets/img/LogoInvert.svg"; ?>" alt="<?php bloginfo("name"); ?>">
					</a>

					<button class="navigation__toggle" v-bind:class="{ 'navigation__toggle--expanded': navigation.expanded }" v-on:click="navigationToggle" title="<?php _e("Menu", "grund"); ?>"></button>
				</div>
			</header>

			<!-- NAVIGATION -->
			<nav class="navigation">
				<ul class="navigation__list" v-bind:class="{ 'navigation__list--expanded': navigation.expanded }">
					<?php grund_navigation("navigation"); ?>
				</ul>
			</nav>

			<script type="text/javascript">
			new Vue({
				el: '#navigation',
				data: {
					navigation: {
						expanded: false
					}
				},
				methods: {
					navigationToggle: function(){
						this.navigation.expanded = !this.navigation.expanded;
					}
				}
			})
			</script>
		</div>
