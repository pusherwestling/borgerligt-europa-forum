<?php get_header(); ?>

	<!--
	/********************************/
	/*	index.php 					*/
	/********************************/
	-->

	<!-- CONTENT -->
	<section class="content">
		<div class="layout__container--flex">
			<!-- ARTICLES -->
			<section class="articles">
				<?php if(is_category()): ?>
					<header class="articles__header">
						<h1 class="articles__headline"><?php single_cat_title("", true); ?></h1>
					</header>
				<?php endif; ?>

				<ul class="articles__list">
					<?php if(is_singular()): ?>
						<?php grund_articles(); ?>
					<?php else: ?>
						<?php grund_articles("preview"); ?>
					<?php endif; ?>
				</ul>
			</section>

			<!-- WIDGETS -->
			<section class="widgets">
				<ul class="widgets__list">
					<?php grund_widgets(); ?>
				</ul>
			</section>
		</div>
	</section>

<?php get_footer(); ?>
