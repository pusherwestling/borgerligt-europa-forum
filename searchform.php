<form role="search" method="get" class="search" action="<?php echo home_url('/'); ?>">
	<input type="search" class="search__field" placeholder="<?php _e('Sök', 'grund'); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="search__button">
		<img src="<?php echo get_template_directory_uri()."/assets/img/Search.svg"; ?>" alt="Sök" />
	</button>
</form>
